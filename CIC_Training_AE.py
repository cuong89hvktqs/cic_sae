import os
import pickle
from loguru import logger
import torch
import torch.optim as optim
from nets.ae import AE
from math import sqrt
import torch.nn as nn
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, TensorDataset
from tensorboardX import SummaryWriter
from common_util import create_dataloader

dataset_name = "cic_ids"
net_type = "ae"
dimension = 80  # khac nhau voi moi tap lenh
num_epochs = 100
landa = 10  # he so tinh loss
lr = 0.001
sae = AE(dimension)
batch_size = 128
path_read_data = "./data_loaders/{}/".format(dataset_name)
path_save_results = "./results/{}/epoch{}_lamda{}/".format(dataset_name, num_epochs, landa)
path_save_logs = "./logs/{}/epoch{}_lamda{}/{}/".format(dataset_name, num_epochs, landa, net_type)
file_name_save_results = net_type + "_" + dataset_name + "_" + str(num_epochs) + "_" + str(landa)
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def calculate_loss(encode, decode, input_x, device_train):
    loss1 = torch.nn.MSELoss()(decode, input_x)
    return loss1


def calculate_loss_v2(encode, decode, input_x, device_train):
    loss1 = torch.nn.MSELoss()(decode, input_x)
    return loss1


def load_data_to_train(path_to_read_data):
    filename_x = path_to_read_data + "X_train.pkl"
    filename_y = path_to_read_data + "y_train.pkl"
    with open(filename_x, "rb") as f:
        X = pickle.load(f)

    with open(filename_y, "rb") as f:
        y = pickle.load(f)
    return X, y


if __name__ == "__main__":

    sae.to(device)
    if not os.path.exists(path_save_results):
        os.mkdir(path_save_results)

    optimizer = optim.Adam(sae.parameters(), lr=0.001)
    patience = 50  # Số epoch không cải thiện trước khi dừng
    best_loss = float('inf')
    no_improvement_count = 0

    X, y = load_data_to_train(path_read_data)  # Doc du lieu nhieu len de huan luyen

    X = torch.tensor(X, dtype=torch.float32)
    y = torch.tensor(y, dtype=torch.float32)

    X = X.to(device)
    y = y.to(device)

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

    print("X shape: ", X_train.shape)

    logdir_tensorboard = path_save_logs
    writer = SummaryWriter(logdir_tensorboard)
    writer.add_graph(sae, X_train)

    dataloader = create_dataloader(X_train, y_train, batch_size)
    sae.train()
    auc_scores = []
    losses = []
    running_loss = 0.0

    for epoch in range(num_epochs):
        for index, batch in enumerate(dataloader):  # DataLoader chứa dữ liệu X_train, y_train
            inputs, labels = batch
            inputs = inputs.to(device)
            labels = labels.to(device)
            encoded, decoded = sae(inputs)
            loss = calculate_loss_v2(encoded, decoded, inputs, device)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            # losses.append(loss)

        writer.add_scalar('training loss', running_loss, epoch)
        # tinh acc val
        encoded, decoded = sae(X_val)
        val_loss = calculate_loss_v2(encoded, decoded, X_val, device)
        writer.add_scalar('Val loss', val_loss, epoch)

        logger.debug(
            "finish epoch {}/{}. Traing Loss={} . Val loss={}".format(epoch, num_epochs, running_loss,
                                                                      val_loss))
        running_loss = 0.0


    # Close TensorBoard writer
    writer.close()
    # Lưu mô hình sau khi huấn luyện
    path_file = path_save_results + file_name_save_results + ".model"
    torch.save(sae.state_dict(), path_file)
