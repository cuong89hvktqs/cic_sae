import os.path
import pickle

from sklearn.model_selection import train_test_split

from nets.ae import AE
from loguru import logger
from tensorboardX import SummaryWriter
import torch
import torch.optim as optim
from sklearn.metrics import roc_auc_score
from common_util import create_dataloader
from math import sqrt
import torch.nn as nn

dataset_name = "cic_ids"
net_type = "sae_two_class"
dimension = 80  # so chieu khac nhau voi moi tap lenh
num_epochs = 100
ratio_noisy_dimension = 1
landa = 10  # he so tinh loss
lr = 0.001
sae = AE(dimension)
batch_size = 128
path_read_data = "./data_loaders/{}/".format(dataset_name)
path_save_results = "./results/{}/epoch{}_lamda{}/ratio{}/".format(dataset_name, num_epochs, landa,
                                                                  int(ratio_noisy_dimension * 100))
path_save_logs = "./logs/{}/epoch{}_lamda{}/{}/ratio{}/".format(dataset_name, num_epochs, landa, net_type,
                                                               int(ratio_noisy_dimension * 100))
file_name = (net_type + "_" + dataset_name + "_" + str(num_epochs) + "_" + str(landa) + "_"
             + str(int(ratio_noisy_dimension * 100)))
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def calculate_loss(encode, decode, input_x, input_y, device):
    loss1 = torch.nn.MSELoss()(decode, input_x)
    list = []
    for i in range(len(input_y)):
        if input_y[i] == 1:
            one_m = torch.ones_like(encode[i]).to(device)
            list.append(one_m)
        else:
            zero_m = torch.zeros_like(encode[i]).to(device)
            list.append(zero_m)
    loss2 = torch.nn.MSELoss()(encode, torch.stack(list, dim=0))
    return loss1 + landa * loss2


def calculate_loss_v2(encode, decode, input_x, input_y, device):
    loss1 = torch.nn.MSELoss()(decode, input_x)
    list = []
    for i in range(len(input_y)):
        if input_y[i] == 1:
            one_m = torch.ones_like(encode[i]).to(device)
            list.append(one_m)
        else:
            zero_m = torch.zeros_like(encode[i]).to(device)
            list.append(zero_m)
    loss2 = torch.nn.MSELoss()(encode, torch.stack(list, dim=0))
    return loss1 + landa * loss2, loss1, loss2


def load_data_to_train(path_to_read, ratio):
    filename_x = path_to_read + "X_noise_"+str(int(ratio*100))+".pkl"
    filename_y = path_to_read + "y_noise_"+str(int(ratio*100))+".pkl"
    logger.debug("Read data froM file: {}".format(filename_x))
    with open(filename_x, "rb") as f:
        X = pickle.load(f)

    with open(filename_y, "rb") as f:
        y = pickle.load(f)
    return X, y


if __name__ == "__main__":
    if not os.path.exists(path_save_results):
        os.makedirs(path_save_results)
    sae.to(device)

    optimizer = optim.Adam(sae.parameters(), lr=0.001)
    patience = 50  # Số epoch không cải thiện trước khi dừng
    best_loss = float('inf')
    no_improvement_count = 0

    X, y = load_data_to_train(path_read_data, ratio_noisy_dimension)  # Doc du lieu nhieu len de huan luyen

    X = torch.tensor(X, dtype=torch.float32)
    y = torch.tensor(y, dtype=torch.float32)

    X = X.to(device)
    y = y.to(device)

    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

    print("X shape: ", X_train.shape)

    logdir_tensorboard = path_save_logs
    writer = SummaryWriter(logdir_tensorboard)
    writer.add_graph(sae, X_train)

    dataloader = create_dataloader(X_train, y_train, batch_size)
    sae.train()
    auc_scores = []
    losses = []
    running_loss = 0.0
    re_loses = 0.0
    two_class_losses = 0.0
    for epoch in range(num_epochs):
        for index, batch in enumerate(dataloader):  # DataLoader chứa dữ liệu X_train, y_train
            inputs, labels = batch
            inputs = inputs.to(device)
            labels = labels.to(device)
            encoded, decoded = sae(inputs)
            loss, re_loss, twoclass_loss = calculate_loss_v2(encoded, decoded, inputs, labels, device)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            re_loses += re_loss.item()
            two_class_losses += twoclass_loss.item()
            # losses.append(loss)

        writer.add_scalar('training loss', running_loss, epoch)
        writer.add_scalars('Training_RE_TwoClass_Loss',
                           {'RE_loss': re_loses, 'Twoclass_loss': landa * two_class_losses}, epoch)

        # tinh acc val
        encoded, decoded = sae(X_val)
        re_0 = (encoded ** 2).sum(1).sqrt()
        one_m = torch.ones_like(encoded)
        re_1 = ((encoded - one_m) ** 2).sum(1).sqrt()
        prediction = (re_0 > re_1).to(torch.int).tolist()
        groundtruth = y_val.tolist()
        val_auc = roc_auc_score(groundtruth, prediction)
        val_loss, val_re_loss, val_twoclass_loss = calculate_loss_v2(encoded, decoded, X_val, y_val, device)
        writer.add_scalar('Val acc', val_auc, epoch)
        writer.add_scalar('Val loss', val_loss, epoch)

        writer.add_scalars('Val_RE_TwoClass_Loss',
                           {'RE_loss': val_re_loss, 'Twoclass_loss': landa * val_twoclass_loss}, epoch)

        logger.debug(
            "finish epoch {}/{}. Traing Loss={} . Val loss={}. Val acc={} ".format(epoch, num_epochs, running_loss,
                                                                                   val_loss, val_auc))
        running_loss = 0.0
        re_loses = 0.0
        two_class_losses = 0.0

    # Close TensorBoard writer
    writer.close()
    # Lưu mô hình sau khi huấn luyện

    path_file = path_save_results + file_name + ".model"
    torch.save(sae.state_dict(), path_file)

    # Vẽ biểu đồ loss qua các epoch

    """ tensor1 = losses.detach().numpy()
    path_file=path_save_results+file_name+".png"
    plt.plot(range(1, len(tensor1) + 1), tensor1)
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.title("Loss during Training")
    plt.grid()
    plt.show()
    plt.savefig(path_file) """
