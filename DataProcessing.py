import pickle
from loguru import logger
import torch
from torch.utils.data import DataLoader, TensorDataset
import os
import numpy as np
from data_function.datasets import cic_ids
from data_function.noise import add_gaussian_noise_and_change_labels_by_rate

dataset_name = "cic_ids"
dimension = 80
ratios_noisy_feature = [0.15, 0.3, 0.5, 1]
train_batch_size = 128
mal_batch_size = 128
test_batch_size = 128
noise_batch_size = 128
default_mdata_loaders_folder_path = "data_loaders"
train_data_loader_pickle_path = "data_loaders/{}/train_data_loader.pickle".format(dataset_name)
mal_data_loader_pickle_path = "data_loaders/{}/mal_data_loader.pickle".format(dataset_name)
test_data_loader_pickle_path = "data_loaders/{}/test_data_loader.pickle".format(dataset_name)
noise_data_loader_pickle_path = "data_loaders/{}/noise_data_loader.pickle".format(dataset_name)


def create_and_save_dataloader(logger, X, y, batch_size, shuffle=True, filename=""):
    """
    Hàm này tạo một DataLoader cho dữ liệu X và y, sau đó lưu DataLoader vào một tệp.

    Parameters:
    - X: Dữ liệu đầu vào
    - y: Nhãn
    - batch_size: Kích thước của các batch
    - shuffle: Sử dụng để xáo trộn dữ liệu (mặc định True)
    - filename: Tên tệp để lưu DataLoader

    Returns:
    """
    X_torch = torch.from_numpy(X).float()
    Y_torch = torch.from_numpy(y).float()

    dataset_save = TensorDataset(X_torch, Y_torch)
    dataloader = DataLoader(dataset_save, batch_size=batch_size, shuffle=shuffle)
    logger.debug("Save data to pickeer: {}".format(filename))
    # Lưu DataLoader vào tệp bằng cách sử dụng pickle
    with open(filename, 'wb') as f:
        pickle.dump(dataloader, f)
    return


if __name__ == "__main__":
    if not os.path.exists("./data_loaders/"+dataset_name+"/"):
        os.mkdir("./data_loaders/"+dataset_name+"/")
    X_train, y_train, X_mal, y_mal, X_test, y_test = cic_ids()
    # Lưu các tập dữ liệu vào các tệp Python .pkl
    with open("./data_loaders/"+dataset_name+"/X_train.pkl", "wb") as f:
        pickle.dump(X_train, f)

    with open("./data_loaders/"+dataset_name+"/y_train.pkl", "wb") as f:
        pickle.dump(y_train, f)

    with open("./data_loaders/"+dataset_name+"/X_test.pkl", "wb") as f:
        pickle.dump(X_test, f)

    with open("./data_loaders/"+dataset_name+"/y_test.pkl", "wb") as f:
        pickle.dump(y_test, f)

    for ratio in ratios_noisy_feature:
        rate_percent = int(ratio * 100)
        full_path_save_noise_data = "./data_loaders/"+dataset_name+"/X_noise_" + str(rate_percent) + ".pkl"
        full_path_save_noise_lable = "./data_loaders/"+dataset_name+"/y_noise_" + str(rate_percent) + ".pkl"
        X_noise, y_noise = add_gaussian_noise_and_change_labels_by_rate(X_train, y_train, dimension, 0.1, ratio)
        with open(full_path_save_noise_data, "wb") as f:
            pickle.dump(X_noise, f)
        with open(full_path_save_noise_lable, "wb") as f:
            pickle.dump(y_noise, f)
        logger.debug("Save data noise rate {} to file: ".format(ratio))
    logger.debug("Finish save data {} to dataloader: ".format(dataset_name))
    print("X train:")
    print(X_train.shape)
    print(y_train.shape)

