
from nets import AE
from sklearn.manifold import TSNE
from sklearn.metrics import roc_auc_score, confusion_matrix
import pickle
import os
import torch
import numpy as np
from common_util import plot_confusion_matrix, draw_scatter

dataset_name = "cic_ids"
net_type = "sae_one_class"
dimension = 80  # khac nhau voi moi tap lenh
num_epochs = 400
landa = 10  # he so tinh loss
lr = 0.001
sae = AE(dimension)
batch_size = 128
path_read_data = "./data_loaders/{}/".format(dataset_name)
path_save_results = "./results/{}/epoch{}_lamda{}/".format(dataset_name,num_epochs, landa)
path_save_logs = "./logs/{}/epoch{}_lamda{}/{}/".format(dataset_name,num_epochs, landa, net_type)
file_name_save_results = net_type + "_" + dataset_name + "_" + str(num_epochs) + "_" + str(landa)
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def load_data_to_test(path_to_read_data):
    filename_x = path_to_read_data + "X_test.pkl"
    filename_y = path_to_read_data + "y_test.pkl"
    with open(filename_x, "rb") as f:
        X = pickle.load(f)

    with open(filename_y, "rb") as f:
        y = pickle.load(f)
    return X, y


def load_data_to_train(path_to_read_data):
    filename_x = path_to_read_data + "X_train.pkl"
    filename_y = path_to_read_data + "y_train.pkl"
    with open(filename_x, "rb") as f:
        X = pickle.load(f)

    with open(filename_y, "rb") as f:
        y = pickle.load(f)
    return X, y


def load_sae_oneclass_model(dimension_model, device_model):
    """
    Load a model from default model file.

    This is used to ensure consistent default model behavior.
    """
    default_model_path = path_save_results + file_name_save_results + ".model"
    model = AE(dimension_model)
    model.to(device_model)
    if os.path.exists(default_model_path):
        try:
            model.load_state_dict(torch.load(default_model_path))
        except:
            print(
                "Couldn't load model from {}".format(default_model_path)
            )
    else:
        print("Could not find model: {}".format(default_model_path))

    return model


def calculate_mean_and_std(data):
    mean_gauss = torch.mean(data)
    std_gauss = torch.std(data)
    return mean_gauss, std_gauss


def is_outside_3_std(data_point, mean, std, threshold=3):
    # Kiểm tra xem điểm dữ liệu có nằm ngoài phạm vi mean + threshold * std hay không
    return (data_point - mean) > threshold * std


def calculate_loss(encode, decode, input_x, device):
    loss1 = torch.nn.MSELoss()(decode, input_x)
    zero_m = torch.zeros(encode.shape).to(device)
    loss2 = torch.nn.MSELoss()(encode, zero_m)
    return loss1, loss2


if __name__ == "__main__":
    if not os.path.exists(path_save_results):
        os.mkdir(path_save_results)
    # Sử dụng GPU nếu có
    model_sae = load_sae_oneclass_model(dimension, device)

    X_test, y_test = load_data_to_test(path_read_data)
    X_test = torch.tensor(X_test, dtype=torch.float32)
    y_test = torch.tensor(y_test, dtype=torch.float32)
    X_test = X_test.to(device)
    y_test = y_test.to(device)

    X_train, y_train = load_data_to_train(path_read_data)  # Doc du lieu nhieu len de huan luyen

    X_train = torch.tensor(X_train, dtype=torch.float32)
    y_train = torch.tensor(y_train, dtype=torch.float32)

    X_train = X_train.to(device)
    y_train = y_train.to(device)
    # truth = []
    # output = []
    #
    # dataloader = create_dataloader(X_test, y_test, batch_size)
    # for index, (batch, labels) in enumerate(dataloader):
    #     encoded, decoded = model_sae(batch)
    #     re_0 = (encoded ** 2).sum(1).sqrt()
    #     one_m = torch.ones_like(encoded)
    #     re_1 = ((encoded - one_m) ** 2).sum(1).sqrt()
    #
    #     prediction = (re_0 > re_1).to(torch.int).tolist()
    #     groundtruth = labels.tolist()
    #     truth = truth + groundtruth
    #     output = output + prediction

    encoded_test, decode_test = model_sae(X_test)
    encode_train, decode_train = model_sae(X_train)
    re_test = (encoded_test ** 2).sum(1).sqrt()
    re_train = (encode_train ** 2).sum(1).sqrt()

    mean, std = calculate_mean_and_std(re_train)
    print("mean")

    prediction = []
    for re_point in re_test:
        if is_outside_3_std(re_point, mean, std, 3):
            prediction.append(1)
        else:
            prediction.append(0)
    groundtruth = y_test.tolist()

    auc = roc_auc_score(groundtruth, prediction)
    fread_file = open(path_save_results + file_name_save_results + "_acc.txt", "w")
    fread_file.write("AUC={}".format(auc))
    fread_file.close()
    confusion_mat = confusion_matrix(groundtruth, prediction)

    path_file = path_save_results + file_name_save_results + "_confusion_mat.png"
    plot_confusion_matrix(confusion_mat, [0, 1], path_file)

    loss_re, loss_0 = calculate_loss(encoded_test, decode_test, X_test, device)

    print(loss_re)
    print(loss_0)

    tsne = TSNE(n_components=2, random_state=42)
    data_test = encoded_test.detach().numpy()
    lable_test = y_test.detach().numpy()

    data_tsne = tsne.fit_transform(data_test)
    path_file = path_save_results + file_name_save_results + "_tsne.png"
    draw_scatter(data_tsne, lable_test, path_file, "LATENT DATA TEST SAE ONECLAS")
