# -*- coding: utf-8 -*-
import itertools

from sklearn.metrics import confusion_matrix, accuracy_score, f1_score, precision_score, recall_score, \
    classification_report, roc_auc_score, accuracy_score, f1_score, auc, roc_curve
from torch.utils.data import DataLoader, TensorDataset
import matplotlib.pyplot as plt
import os
import numpy as np



def create_dataloader(X, y, batch_size_data, shuffle=True):
    dataset = TensorDataset(X, y)
    dataloader = DataLoader(dataset, batch_size=batch_size_data, shuffle=shuffle)
    return dataloader


def plot_confusion_matrix(cm, class_names, name_file):
    """
    Returns a matplotlib figure containing the plotted confusion matrix.

    Args:
       cm (array, shape = [n, n]): a confusion matrix of integer classes
       class_names (array, shape = [n]): String names of the integer classes
    """

    figure = plt.figure(figsize=(8, 8))
    plt.imshow(cm, interpolation='nearest')
    plt.title("Confusion matrix")
    plt.colorbar()
    tick_marks = np.arange(len(class_names))
    plt.xticks(tick_marks, class_names, rotation=45)
    plt.yticks(tick_marks, class_names)

    # Normalize the confusion matrix.
    cm = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=2)

    # Use white text if squares are dark; otherwise black.
    threshold = cm.max() / 2.

    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        color = "white" if cm[i, j] > threshold else "black"
        plt.text(j, i, cm[i, j], horizontalalignment="center", color=color)

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.savefig(name_file)


# Draw confusion_matrix
def draw_confusion_matrix(ytest_class, y_pred_class, label):
    ConfusionMatrix = confusion_matrix(ytest_class, y_pred_class)
    print('Confusion matrix:\n{}\n'.format(ConfusionMatrix))
    test_acc = accuracy_score(ytest_class, y_pred_class)
    test_f1 = f1_score(ytest_class, y_pred_class, average=None)
    test_precision = precision_score(ytest_class, y_pred_class, average=None)
    test_recall = recall_score(ytest_class, y_pred_class, average=None)
    print("accuracy:", test_acc)
    print("f1 score:", test_f1)
    print("precision:", test_precision)
    print("recall:", test_recall)

    report = classification_report(ytest_class, y_pred_class, target_names=label)
    print(report)


def compute_ACC_AUC_F1_score(y_class, y_pred, y_score):
    AUC_score = round(roc_auc_score(y_class, y_score), 4)
    ACC_score = round(accuracy_score(y_class, y_pred), 4)
    F1_score = round(f1_score(y_class, y_pred), 4)
    print("ACC={}, AUC={}, F1={} ".format(ACC_score, AUC_score, F1_score))
    return ACC_score, AUC_score, F1_score


def draw_scatter(X_2d, y, file_save_name_path, title_name):
    plt.clf()
    target_ids = [0, 1]
    colors = 'b', 'r',
    label = "Benign", "Malware"
    plt.title(title_name)
    for i, c, label in zip(target_ids, colors, label):
        plt.scatter(X_2d[y == i, 0], X_2d[y == i, 1], c=c, label=label)
    plt.legend()
    plt.savefig(file_save_name_path)


def draw_ROC_cureve_algorithm(y_test, y_score_AE, sub_folder_output, roc_fig_name):
    # Compute ROC curve and ROC area for each class

    fpr_AE, tpr_AE, thresholds_AE = roc_curve(y_test, y_score_AE, pos_label=1)  # positive label=1

    plt.clf()
    plt.figure()
    # lw = 2
    plt.plot(fpr_AE, tpr_AE, color='green',
             label='AE (AUC = %0.4f)' % auc(fpr_AE, tpr_AE))

    plt.plot([0, 1], [0, 1], color='navy', linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend()
    plt.savefig(os.path.join(sub_folder_output, roc_fig_name))
    plt.draw()
