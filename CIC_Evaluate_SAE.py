from nets.ae import AE
from sklearn.manifold import TSNE
from sklearn.metrics import accuracy_score, roc_auc_score, confusion_matrix, roc_curve, auc
import matplotlib.pyplot as plt
import pickle
import os
import torch
import numpy as np
from common_util import plot_confusion_matrix, draw_scatter

dataset_name = "cic_ids"
net_type = "sae_two_class"
dimension = 80  # so chieu khac nhau voi moi tap lenh
num_epochs = 400
ratio_moisy_dimension = 1
landa = 10  # he so tinh loss
lr = 0.001
sae = AE(dimension)
batch_size = 128
path_read_data = "./data_loaders/{}/".format(dataset_name)
path_save_results = "./results/{}/epoch{}_lamda{}/ratio{}/".format(dataset_name, num_epochs, landa,
                                                                  int(ratio_moisy_dimension * 100))
path_save_logs = "./logs/{}/epoch{}_lamda{}/{}/ratio{}/".format(dataset_name, num_epochs, landa, net_type,
                                                               int(ratio_moisy_dimension * 100))
file_name = (net_type + "_" + dataset_name + "_" + str(num_epochs) + "_" + str(landa) + "_"
             + str(int(ratio_moisy_dimension * 100)))
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def load_data_to_test(path_to_read_data):
    filename_x = path_to_read_data + "X_test.pkl"
    filename_y = path_to_read_data + "y_test.pkl"
    with open(filename_x, "rb") as f:
        X = pickle.load(f)

    with open(filename_y, "rb") as f:
        y = pickle.load(f)
    return X, y


def evaluate_model(y_lable, y_pred_prob):
    # Tính ROC Curve và AUC
    fpr, tpr, thresholds = roc_curve(y_lable, y_pred_prob)
    roc_auc = auc(fpr, tpr)

    # Vẽ ROC Curve
    plt.figure(figsize=(8, 6))
    plt.plot(fpr, tpr, color='darkorange', lw=2, label='ROC curve (AUC = {:.2f})'.format(roc_auc))
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic (ROC) Curve')
    plt.legend(loc='lower right')
    plt.show()
    plt.savefig('ROC_500.png')

    # Chọn ngưỡng dựa trên điểm cắt ROC Curve (ví dụ: ngưỡng tại điểm tpr - fpr min)
    best_threshold_index = np.argmax(tpr - fpr)
    best_threshold = thresholds[best_threshold_index]

    # Dự đoán nhãn dựa trên ngưỡng
    y_pred = (y_pred_prob >= best_threshold).astype(int)

    # Tính Accuracy
    acc = accuracy_score(y_lable, y_pred)

    # Tính Confusion Matrix
    conf_matrix = confusion_matrix(y_lable, y_pred)
    print(f'Accuracy: {acc}')
    print(f'Confusion Matrix:\n{conf_matrix}')


def load_sae_model(dimension_model, device_model):
    """
    Load a model from default model file.

    This is used to ensure consistent default model behavior.
    """
    default_model_path = path_save_results + file_name + ".model"
    model = AE(dimension_model)
    model.to(device_model)
    if os.path.exists(default_model_path):
        try:
            model.load_state_dict(torch.load(default_model_path))
        except:
            print(
                "Couldn't load model from {}".format(default_model_path)
            )
    else:
        print("Could not find model: {}".format(default_model_path))

    return model


if __name__ == "__main__":
    if not os.path.exists(path_save_results):
        os.makedirs(path_save_results)

    # Sử dụng GPU nếu có

    model_sae = load_sae_model(dimension, device)

    X_test, y_test = load_data_to_test(path_read_data)
    X_test = torch.tensor(X_test, dtype=torch.float32)
    y_test = torch.tensor(y_test, dtype=torch.float32)
    X_test = X_test.to(device)
    y_test = y_test.to(device)

    # truth = []
    # output = []
    #
    # dataloader = create_dataloader(X_test, y_test, batch_size)
    # for index, (batch, labels) in enumerate(dataloader):
    #     encoded, decoded = model_sae(batch)
    #     re_0 = (encoded ** 2).sum(1).sqrt()
    #     one_m = torch.ones_like(encoded)
    #     re_1 = ((encoded - one_m) ** 2).sum(1).sqrt()
    #
    #     prediction = (re_0 > re_1).to(torch.int).tolist()
    #     groundtruth = labels.tolist()
    #     truth = truth + groundtruth
    #     output = output + prediction

    encoded, decoded = model_sae(X_test)
    re_0 = (encoded ** 2).sum(1).sqrt()
    one_m = torch.ones_like(encoded)
    re_1 = ((encoded - one_m) ** 2).sum(1).sqrt()
    prediction = (re_0 > re_1).to(torch.int).tolist()
    groundtruth = y_test.tolist()

    auc = roc_auc_score(groundtruth, prediction)
    fread_file = open(path_save_results + file_name + "_acc.txt", "w")
    fread_file.write("AUC={}".format(auc))
    fread_file.close()

    confusion_mat = confusion_matrix(groundtruth, prediction)

    path_file = path_save_results + file_name + "_confusion_mat.png"
    plot_confusion_matrix(confusion_mat, [0, 1], path_file)

    tsne = TSNE(n_components=2, random_state=42)
    data_test = encoded.detach().numpy()
    lable_test = y_test.detach().numpy()

    data_tsne = tsne.fit_transform(data_test)
    path_file = path_save_results + file_name + "_tsne.png"
    draw_scatter(data_tsne, lable_test, path_file, "LATENT DATA TEST SAE ")
