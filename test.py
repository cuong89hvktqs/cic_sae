import numpy as np
from  data_function.noise import add_gaussian_noise_and_change_labels_by_rate


if __name__ == "__main__":
    random_data=np.random.rand(20,10)
    y_lable=np.zeros(20)
    print("Data:")
    print(random_data)
    print("Lable ban dau:")
    print(y_lable)
    x_noise, y_noise=add_gaussian_noise_and_change_labels_by_rate(random_data,y_lable,10,0.4,1)
    print("Data noise :")
    print(x_noise)
    print("Lable noise:")
    print(y_noise)