import numpy


def add_gaussian_noise_and_change_labels(X, Y, noise_ratio=0.1):
    """
    Hàm này tạo bộ dữ liệu nhiễu bằng cách thêm nhiễu Gaussian noise và đổi nhãn dữ liệu nhiễu thành 1.

    Parameters:
    - X: Dữ liệu đầu vào
    - Y: Nhãn đầu vào (0 cho lớp dương)
    - noise_ratio: Tỷ lệ của dữ liệu nhiễu (ví dụ: tỷ lệ 10%)
    - noise_std: Độ lệch chuẩn của noise, điều chỉnh tùy theo mức độ nhiễu mong muốn

    Returns:
    - noisy_X: Dữ liệu nhiễu
    - noisy_Y: Nhãn dữ liệu nhiễu (1 cho lớp dương)
    """
    num_samples = X.shape[0]
    num_noisy_samples = int(noise_ratio * num_samples)
    means = numpy.mean(X, axis=0, dtype=numpy.float64)
    std_dev_values = numpy.std(X, axis=0, dtype=numpy.float64)
    print(means)
    print(std_dev_values)

    # Chọn ngẫu nhiên các mẫu để thêm nhiễu và đổi nhãn
    indices_to_noisify = numpy.random.choice(num_samples, num_noisy_samples, replace=False)

    # Sao chép dữ liệu đầu vào và nhãn
    noisy_X = numpy.copy(X)
    noisy_Y = numpy.copy(Y)

    # Thêm nhiễu Gaussian noise vào các mẫu đã chọn
    noisy_X[indices_to_noisify] = numpy.random.normal(means, std_dev_values, noisy_X[indices_to_noisify].shape)
    for i in range(len(indices_to_noisify)):
        for j in range(len(means)):
            if noisy_X[indices_to_noisify[i]][j] >= means[j]:
                noisy_X[indices_to_noisify[i]][j] += 3 * std_dev_values[j]
            else:
                noisy_X[indices_to_noisify[i]][j] -= 3 * std_dev_values[j]
                # Thay đổi nhãn của các mẫu nhiễu thành 1
    noisy_Y[indices_to_noisify] = 1

    return noisy_X, noisy_Y


def add_gaussian_noise_and_change_labels_by_rate(X, Y, dimension, noise_ratio_sample=0.1, noise_ratio_feature=0.15):
    """
    Hàm này tạo bộ dữ liệu nhiễu bằng cách thêm nhiễu Gaussian noise và đổi nhãn dữ liệu nhiễu thành 1.

    Parameters:
    - X: Dữ liệu đầu vào
    - Y: Nhãn đầu vào (0 cho lớp dương)
    - noise_ratio: Tỷ lệ của dữ liệu nhiễu (ví dụ: tỷ lệ 10%)
    - noise_std: Độ lệch chuẩn của noise, điều chỉnh tùy theo mức độ nhiễu mong muốn

    Returns:
    - noisy_X: Dữ liệu nhiễu
    - noisy_Y: Nhãn dữ liệu nhiễu (1 cho lớp dương)
    """
    num_samples = X.shape[0]
    num_noisy_samples = int(noise_ratio_sample * num_samples)
    num_noisy_feature = int(noise_ratio_feature * dimension)
    means = numpy.mean(X, axis=0, dtype=numpy.float64)
    std_dev_values = numpy.std(X, axis=0, dtype=numpy.float64)
    '''print(means)
    print(std_dev_values)'''

    # Chọn ngẫu nhiên các mẫu để thêm nhiễu và đổi nhãn
    indices_to_noisify = numpy.random.choice(num_samples, num_noisy_samples, replace=False)
    # Sao chép dữ liệu đầu vào và nhãn
    noisy_X = numpy.copy(X)
    noisy_Y = numpy.copy(Y)

    # Thêm nhiễu Gaussian noise vào các mẫu đã chọn
    noisy_X[indices_to_noisify] = numpy.random.normal(means, std_dev_values, noisy_X[indices_to_noisify].shape)
    for indices_noise in indices_to_noisify:
        feature_indi_to_noisify = numpy.random.choice(dimension, num_noisy_feature, replace=False)
        for index, indi_feature in enumerate(feature_indi_to_noisify):
            if noisy_X[indices_noise][indi_feature] >= means[index]:
                X[indices_noise][indi_feature] += 3 * std_dev_values[index]
            else:
                X[indices_noise][indi_feature] -= 3 * std_dev_values[index]
                # Thay đổi nhãn của các mẫu nhiễu thành 1
    noisy_Y[indices_to_noisify] = 1

    return X, noisy_Y
